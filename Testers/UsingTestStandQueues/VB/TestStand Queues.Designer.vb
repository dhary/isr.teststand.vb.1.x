<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UseTSQueue
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.label1 = New System.Windows.Forms.Label
        Me.QueueElement = New System.Windows.Forms.TextBox
        Me.DequeueButton = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Location = New System.Drawing.Point(85, 76)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(110, 13)
        Me.label1.TabIndex = 5
        Me.label1.Text = "Queue Element Value"
        '
        'QueueElement
        '
        Me.QueueElement.Location = New System.Drawing.Point(87, 92)
        Me.QueueElement.Name = "QueueElement"
        Me.QueueElement.Size = New System.Drawing.Size(98, 20)
        Me.QueueElement.TabIndex = 4
        '
        'DequeueButton
        '
        Me.DequeueButton.Location = New System.Drawing.Point(87, 144)
        Me.DequeueButton.Name = "DequeueButton"
        Me.DequeueButton.Size = New System.Drawing.Size(100, 34)
        Me.DequeueButton.TabIndex = 3
        Me.DequeueButton.Text = "Dequeue Element"
        Me.DequeueButton.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.QueueElement)
        Me.Controls.Add(Me.DequeueButton)
        Me.Name = "Form1"
        Me.Text = "Using TestStand Queues"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents label1 As System.Windows.Forms.Label
    Private WithEvents QueueElement As System.Windows.Forms.TextBox
    Private WithEvents DequeueButton As System.Windows.Forms.Button

End Class
