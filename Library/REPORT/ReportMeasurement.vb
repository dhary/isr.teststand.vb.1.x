﻿''' <summary>
''' Parses Test Stand report measurement.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="06/16/2011" by="David Hary" revision="1.0.4184.x">
''' Created
''' </history>
Public Class ReportMeasurement

#Region " CONSTRUCTORS AND DESTRACTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ReportParser" /> class.
    ''' </summary>
    Public Sub New(ByVal location As Integer, ByVal records As List(Of String))
        MyBase.new()
        Me._LastLocation = location
        _records = records
        Me.ErrorMessage = ""
    End Sub

#End Region

#Region " DATA RETRIEVAL "

    ''' <summary>
    ''' Returns the specified Measurement element key.
    ''' </summary>
    ''' <param name="value">The element.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function MeasurementElementKey(ByVal value As MeasurementDataElement) As String
        Dim key As String = value.ToString()
        Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(key)
        Dim attributes As ComponentModel.DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(ComponentModel.DescriptionAttribute), False), ComponentModel.DescriptionAttribute())
        If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
            key = attributes(0).Description
        End If
        Return key
    End Function

    Private _MeasurementRecords As Dictionary(Of String, String)
    ''' <summary>
    ''' Returns the specified Measurement element value.
    ''' </summary>
    ''' <param name="value">The element.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function MeasurementElementValue(ByVal value As MeasurementDataElement) As String
        Dim v As String = ""
        If _MeasurementRecords IsNot Nothing AndAlso _MeasurementRecords.Count > 0 Then
            _MeasurementRecords.TryGetValue(Me.MeasurementElementKey(value), v)
        End If
        Return v
    End Function

    ''' <summary>
    ''' Determines whether this instance has records.
    ''' </summary>
    ''' <returns><c>true</c> if this instance has records; otherwise, <c>false</c>.</returns>
    ''' <remarks></remarks>
    Public Function HasRecords() As Boolean
        Return Me._MeasurementRecords IsNot Nothing AndAlso Me._MeasurementRecords.Count > 0
    End Function

    ''' <summary>
    ''' Gets or sets the module name.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Name As String

#End Region

#Region " PARSE"

    ''' <summary>
    ''' Gets or sets the error sentinel
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ErrorOccurred As Boolean

    ''' <summary>
    ''' Gets or sets the error message.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ErrorMessage As String

    ''' <summary>
    ''' The last location parsed.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property LastLocation As Integer

    Private _records As List(Of String)
    ''' <summary>
    ''' Gets the record.
    ''' </summary>
    ''' <remarks></remarks>
    Public ReadOnly Property Record As String
        Get
            Return Me._records(_LastLocation)
        End Get
    End Property

    ''' <summary>
    ''' Gets the next record.
    ''' </summary>
    ''' <remarks></remarks>
    Public ReadOnly Property NextRecord As String
        Get
            Return Me._records(_LastLocation + 1)
        End Get
    End Property

    ''' <summary>
    ''' Determines whether [is end measurement].
    ''' </summary>
    ''' <returns><c>true</c> if [is end measurement]; otherwise, <c>false</c>.</returns>
    ''' <remarks></remarks>
    Public Function IsEndMeasurement() As Boolean
        Return Me.Record.Trim.StartsWith("Status:", StringComparison.OrdinalIgnoreCase) OrElse Me.Record.Trim.StartsWith("Module Time:", StringComparison.OrdinalIgnoreCase)
    End Function

    ''' <summary>
    ''' Lists the measurements.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ListMeasurements() As String
        Dim builder As New Text.StringBuilder
        If Me.HasRecords Then
            For Each r As KeyValuePair(Of String, String) In Me._MeasurementRecords
                If builder.Length > 0 Then
                    builder.AppendLine()
                End If
                builder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "      {0}: {1}", r.Key, r.Value)
            Next
        End If
        Return builder.ToString
    End Function

    ''' <summary>
    ''' Parses this instance.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Parse() As Boolean
        Me.ErrorMessage = ""
        Me.ErrorOccurred = False
        Me._MeasurementRecords = New Dictionary(Of String, String)
        Dim keyValue As KeyValuePair(Of String, String) = ReportModule.ParseRecord(Me.Record)
        Me.Name = keyValue.Key
        If Not String.IsNullOrEmpty(keyValue.Value) Then
            Me._MeasurementRecords.Add("Status", keyValue.Value)
        End If
        Do
            Me.LastLocation += 1
            Dim r As String = Me.Record
            If Not String.IsNullOrEmpty(r) Then
                keyValue = ReportModule.ParseRecord(r)
                If Not String.IsNullOrEmpty(keyValue.Value) Then
                    If keyValue.Key.Equals("Measurement") Then
                        Me._MeasurementRecords.Add("Data", keyValue.Value)
                    Else
                        Me._MeasurementRecords.Add(keyValue.Key, keyValue.Value)
                    End If
                End If
            End If
        Loop Until (Me.IsEndMeasurement)
        Return Me.HasRecords
    End Function

#End Region

End Class

''' <summary>
''' Enumerates thw measurement data elements
''' </summary>
''' <remarks></remarks>
Public Enum MeasurementDataElement
    <ComponentModel.Description("None")> None
    <ComponentModel.Description("Measurement")> Measurement
    <ComponentModel.Description("Units")> Units
    <ComponentModel.Description("Low")> LowLimit
    <ComponentModel.Description("High")> HighLimit
    <ComponentModel.Description("Comparison Type")> ComparisonType
    <ComponentModel.Description("Module Time")> ModuleTime
    <ComponentModel.Description("Data")> Data
    <ComponentModel.Description("Status")> Status

End Enum