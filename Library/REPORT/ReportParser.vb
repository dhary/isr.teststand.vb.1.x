﻿''' <summary>
''' Parses Test Stand reports.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="06/16/2011" by="David Hary" revision="1.0.4184.x">
''' Created
''' </history>
Public Class ReportParser

    Implements IDisposable

#Region " CONSTRUCTORS AND DESTRACTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ReportParser" /> class.
    ''' </summary>
    Public Sub New()
        MyBase.new()
        lastFileName = ""
        Me.ErrorMessage = ""
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method overridable (virtual) because a derived 
    ''' class should not be able to override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return _isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            _isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by its disposing parameter.
    ''' If True, the method has been called directly or indirectly by a user's code--managed 
    ''' and unmanaged resources can be disposed. If disposing equals False, the method has been 
    ''' called by the runtime from inside the finalizer and you should not reference other 
    ''' objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                ' Free shared managed resources
                If disposing Then


                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method does not get called. 
    ''' It gives the base class the opportunity to finalize. Do not provide destructors 
    ''' in types derived from this class.
    ''' </summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here. Calling Dispose(false) is optimal in terms of
        ' readability and maintainability.
        Dispose(False)
        ' The compiler automatically adds a call to the base class finalizer 
        ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
        MyBase.Finalize()
    End Sub

#End Region

#Region " DATA RETRIEVAL "

    ''' <summary>
    ''' Returns the specified Header element key.
    ''' </summary>
    ''' <param name="value">The element.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function HeaderElementKey(ByVal value As HeaderElement) As String
        Dim key As String = value.ToString()
        Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(key)
        Dim attributes As ComponentModel.DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(ComponentModel.DescriptionAttribute), False), ComponentModel.DescriptionAttribute())
        If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
            key = attributes(0).Description
        End If
        Return key
    End Function

    Private _headerRecords As Dictionary(Of String, String)
    ''' <summary>
    ''' Returns the specified Header element value.
    ''' </summary>
    ''' <param name="value">The element.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function HeaderElementValue(ByVal value As HeaderElement) As String
        Dim v As String = ""
        If _headerRecords IsNot Nothing AndAlso _headerRecords.Count > 0 Then
            _headerRecords.TryGetValue(Me.HeaderElementKey(value), v)
        End If
        Return v
    End Function

    Private _modules As Dictionary(Of String, ReportModule)
    ''' <summary>
    ''' Selects the module element
    ''' </summary>
    ''' <param name="key"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ModuleElementGetter(ByVal key As String) As ReportModule
        If _modules IsNot Nothing AndAlso _modules.Count > 0 Then
            If _modules.ContainsKey(key) Then
                Return _modules(key)
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

#End Region

#Region " PARSE "

    ''' <summary>
    ''' Gets or sets the error sentinel
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ErrorOccurred As Boolean

    ''' <summary>
    ''' Gets or sets the error message.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ErrorMessage As String

    ''' <summary>
    ''' Gets the sentinel indicating that a report exits.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property HasReport As Boolean

    Const minimumRecordCount As Integer = 10
    ''' <summary>
    ''' Gets the sentinel indicating that we have records.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function HasRecords() As Boolean
        Return Me._records IsNot Nothing AndAlso Me._records.Count > minimumRecordCount
    End Function

    Private _records As List(Of String)
    ''' <summary>
    ''' Splits the file records.
    ''' </summary>
    ''' <param name="fileName">Name of the file.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SplitFileRecords(ByVal fileName As String) As Boolean
        Me.LastRecordNumber = 0
        Me.ErrorMessage = ""
        Me.ErrorOccurred = False
        _records = New List(Of String)
        Dim fi As New IO.FileInfo(fileName)
        If fi.Exists Then
            Dim reader As New IO.StreamReader(fileName)
            Try
                _records = New List(Of String)
                Do Until reader.EndOfStream
                    _records.Add(reader.ReadLine)
                Loop
            Catch
                Throw
            Finally
                reader.Close()
            End Try
            Return True
        Else
            Me.ErrorMessage = "FILE NOT FOUND"
            Me.ErrorOccurred = True
            Return False
        End If
    End Function

    ''' <summary>
    ''' Gets the sentinel indicating that the last report was parsed from the file. 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsDone() As Boolean
        Return Me._records IsNot Nothing AndAlso Me._records.Count <= LastRecordNumber + 1
    End Function

    ''' <summary>
    ''' Gets the last record number that was processed.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property LastRecordNumber As Integer

    ''' <summary>
    ''' Determines whether [is report start].
    ''' </summary>
    ''' <returns><c>true</c> if [is report start]; otherwise, <c>false</c>.</returns>
    ''' <remarks></remarks>
    Private Function isReportStart() As Boolean
        Return Me._records.Item(LastRecordNumber).StartsWith("UUT Report", StringComparison.OrdinalIgnoreCase)
    End Function

    ''' <summary>
    ''' Determines whether [is report end].
    ''' </summary>
    ''' <returns><c>true</c> if [is report end]; otherwise, <c>false</c>.</returns>
    ''' <remarks></remarks>
    Private Function isReportEnd() As Boolean
        Return (Me._records.Count <= Me.LastRecordNumber) OrElse Me._records.Item(LastRecordNumber).StartsWith("End UUT Report", StringComparison.OrdinalIgnoreCase)
    End Function

    ''' <summary>
    ''' Determines whether [is header end].
    ''' </summary>
    ''' <returns><c>true</c> if [is header end]; otherwise, <c>false</c>.</returns>
    ''' <remarks></remarks>
    Private Function isHeaderEnd() As Boolean
        Return Me._records.Item(LastRecordNumber).StartsWith("********************************************************************************", StringComparison.OrdinalIgnoreCase) OrElse
               Me._records.Item(LastRecordNumber).StartsWith("Failure Chain:", StringComparison.OrdinalIgnoreCase)
    End Function

    ''' <summary>
    ''' Finds the report start.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function findReportStart() As Boolean
        Me.ErrorMessage = ""
        Me.ErrorOccurred = False
        If Not HasRecords() Then
            Me.ErrorMessage = "MUST SPLIT FILE RECORDS FIRST"
            Me.ErrorOccurred = True
            Return False
        End If
        Do Until Me.IsDone Or Me.isReportStart
            LastRecordNumber += 1
        Loop
        Return Not IsDone()
    End Function

    ''' <summary>
    ''' Finds the start of the next module.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function findNextTopLevelModule() As Boolean
        Me.ErrorMessage = ""
        Me.ErrorOccurred = False
        If Not HasRecords() Then
            Me.ErrorMessage = "MUST SPLIT FILE RECORDS FIRST"
            Me.ErrorOccurred = True
            Return False
        End If
        Do Until Me.isReportEnd OrElse ReportModule.IsTopLevelModuleStart(Me._records, Me.LastRecordNumber)
            LastRecordNumber += 1
        Loop
        Return Not Me.isReportEnd()
    End Function

    Const headerRecordValuePosition As Integer = 31
    ''' <summary>
    ''' Parses the header.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ParseHeader() As Boolean
        _headerRecords = New Dictionary(Of String, String)
        If Me.isReportStart Then
            LastRecordNumber += 1
        End If
        Do Until Me.isHeaderEnd
            If Not String.IsNullOrEmpty(Me._records.Item(LastRecordNumber).Trim) Then
                Dim element As New ReportElement(Me._records.Item(LastRecordNumber), headerRecordValuePosition)
                If element.ErrorOccurred Then
                    Me.ErrorMessage = element.ErrorMessage
                    Me.ErrorOccurred = True
                    Return False
                Else
                    _headerRecords.Add(element.Key, element.Value)
                End If
            End If
            LastRecordNumber += 1
        Loop
        Return True
    End Function

    ''' <summary>
    ''' Lists the data.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ListData() As String
        Dim builder As New Text.StringBuilder
        If Me._modules IsNot Nothing AndAlso Me._modules.Count > 0 Then
            For Each m As KeyValuePair(Of String, ReportModule) In Me._modules
                builder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}:", m.Key)
                builder.AppendLine()
                builder.Append(m.Value.ListData)
                builder.AppendLine()
            Next
        End If
        Return builder.ToString
    End Function


    Dim lastFileName As String
    ''' <summary>
    ''' Parses the next report from the file.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ParseNext(ByVal fileName As String) As Boolean
        Me.HasReport = False
        Me.ErrorMessage = ""
        Me.ErrorOccurred = False
        If Not fileName.Equals(lastFileName, StringComparison.OrdinalIgnoreCase) Then
            Me.LastRecordNumber = 0
            lastFileName = fileName
            Me.SplitFileRecords(fileName)
        End If
        If Not HasRecords() Then
            Me.ErrorMessage = "MUST SPLIT FILE RECORDS FIRST"
            Me.ErrorOccurred = True
            Return False
        End If
        If Not findReportStart() Then
            Return False
        End If
        If Not ParseHeader() Then
            Return False
        End If
        Me.HasReport = True
        _modules = New Dictionary(Of String, ReportModule)
        Do Until Me.isReportEnd
            If Me.findNextTopLevelModule() Then
                Dim m As New ReportModule(Me.LastRecordNumber, Me._records)
                If m.Parse Then
                    _modules.Add(m.Name, m)
                End If
                Me.LastRecordNumber = m.LastLocation
            End If
        Loop
        Return True
    End Function

#End Region


End Class

Public Enum LocationStates
    None = 0
    InReport = 1
    InHeader = 2
    InModule = 3

End Enum
''' <summary>
''' Enumerates teh header elements
''' </summary>
''' <remarks></remarks>
Public Enum HeaderElement
    <ComponentModel.Description("None")> None
    <ComponentModel.Description("Station ID")> StationID
    <ComponentModel.Description("Serial Number")> SerialNumber
    <ComponentModel.Description("Date")> ReportDate
    <ComponentModel.Description("Time")> ReportTime
    <ComponentModel.Description("Operator")> OperatorName
    <ComponentModel.Description("Execution Time")> ExecutionTime
    <ComponentModel.Description("Number of Results")> NumberOfResults
    <ComponentModel.Description("UUT Result")> UUTResult
End Enum