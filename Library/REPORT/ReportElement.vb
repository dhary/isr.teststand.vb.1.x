﻿''' <summary>
''' The report element.
''' The report element consists of a key value pair.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="06/16/2011" by="David Hary" revision="1.0.4184.x">
''' Created
''' </history>
Public Class ReportElement

#Region " CONSTRUCTORS AND DESTRACTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ReportElement" /> class.
    ''' </summary>
    ''' <param name="record">The record.</param>
    ''' <param name="valueColumnPosition">The value column position.</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal record As String, ByVal valueColumnPosition As Short)
        MyBase.new()
        Me.Parse(record, valueColumnPosition)
    End Sub

#End Region

#Region " MEMBERS "

    ''' <summary>
    ''' Gets or sets the key.
    ''' </summary>
    ''' <remarks></remarks>
    Public Property Key As String

    ''' <summary>
    ''' Gets or sets the value.
    ''' </summary>
    ''' <remarks></remarks>
    Public Property Value As String

#End Region

#Region " PARSER "

    ''' <summary>
    ''' Gets or sets the error sentinel
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ErrorOccurred As Boolean

    ''' <summary>
    ''' Gets or sets the error message.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ErrorMessage As String

    ''' <summary>
    ''' Parses the specified record.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="valueColumnPosition">The value column position.</param>
    ''' <returns></returns>
    ''' <remarks>Parses a record of the format: key: value, where the value begins at the specified column.</remarks>
    Public Function Parse(ByVal value As String, ByVal valueColumnPosition As Short) As Boolean
        ErrorMessage = ""
        ErrorOccurred = False
        If String.IsNullOrEmpty(value.Trim) Then
            ErrorMessage = "Empty record"
            ErrorOccurred = True
            Return False
        End If
        If valueColumnPosition <= 1 OrElse valueColumnPosition >= value.Length Then
            ErrorMessage = "Invalid value column position"
            ErrorOccurred = True
        End If
        Me.Key = value.Substring(0, value.IndexOf(":"c))
        Me.Value = value.Substring(valueColumnPosition - 1)
        Return ErrorOccurred
    End Function

#End Region


End Class
