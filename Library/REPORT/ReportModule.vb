﻿''' <summary>
''' Parses Test Stand report module.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="06/16/2011" by="David Hary" revision="1.0.4184.x">
''' Created
''' </history>
Public Class ReportModule

#Region " CONSTRUCTORS AND DESTRACTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ReportParser" /> class.
    ''' </summary>
    Public Sub New(ByVal location As Integer, ByVal records As List(Of String))
        MyBase.new()
        Me._LastLocation = location
        _records = records
        Me.ErrorMessage = ""
    End Sub

#End Region

#Region " SHARED "

    ''' <summary>
    ''' Parses the record.
    ''' </summary>
    ''' <param name="record">The record.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ParseRecord(ByVal record As String) As KeyValuePair(Of String, String)
        Dim key As String = ""
        Dim value As String = ""
        Dim colonIndex As Integer = record.IndexOf(": ")
        If colonIndex > 0 Then
            key = record.Substring(0, colonIndex).Trim
            If colonIndex < record.Length - 1 Then
                value = record.Substring(colonIndex + 2).Trim
            End If
        ElseIf record.EndsWith(":") Then
            key = record.Substring(0, record.Length - 1).Trim
        End If
        Return New KeyValuePair(Of String, String)(key, value)
    End Function

    ''' <summary>
    ''' Determines whether [is module start] [the specified record].
    ''' </summary>
    ''' <param name="records">The records.</param>
    ''' <returns><c>true</c> if [is module start] [the specified record]; otherwise, <c>false</c>.</returns>
    ''' <remarks></remarks>
    Public Shared Function IsTopLevelModuleStart(ByVal records As List(Of String), ByVal location As Integer) As Boolean
        Dim record As String = records.Item(location)
        If Not record.StartsWith("   ") Then
            Dim keyValue As KeyValuePair(Of String, String) = ParseRecord(record)
            If String.IsNullOrEmpty(keyValue.Value) Then
                Return False
            Else
                Return keyValue.Value.StartsWith("P") OrElse keyValue.Value.StartsWith("F")
            End If
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Determines whether [is module start] [the specified record].
    ''' </summary>
    ''' <param name="records">The records.</param>
    ''' <returns><c>true</c> if [is module start] [the specified record]; otherwise, <c>false</c>.</returns>
    ''' <remarks></remarks>
    Public Shared Function IsModuleStart(ByVal records As List(Of String), ByVal location As Integer) As Boolean
        Dim record As String = records.Item(location)
        If Not record.StartsWith("   ") Then
            Dim keyValue As KeyValuePair(Of String, String) = ParseRecord(record)
            If String.IsNullOrEmpty(keyValue.Value) Then
                Return False
            Else
                Return keyValue.Value.StartsWith("P") OrElse keyValue.Value.StartsWith("F")
            End If
        Else
            Return False
        End If
    End Function

#End Region

#Region " RECORDS "

    Private _records As List(Of String)
    Const minimumRecordCount As Integer = 10
    ''' <summary>
    ''' Gets the sentinel indicating that we have records.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function HasRecords() As Boolean
        Return Me._records IsNot Nothing AndAlso Me._records.Count > minimumRecordCount
    End Function

    ''' <summary>
    ''' The last location parsed.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property LastLocation As Integer

#End Region

#Region " DATA "

    Private _ModuleRecords As Dictionary(Of String, String)

    ''' <summary>
    ''' Gets or sets the module name.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Name As String

    Private _measurements As Dictionary(Of String, ReportMeasurement)
    ''' <summary>
    ''' Selects the measurement element
    ''' </summary>
    ''' <param name="key"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function MeasurementElementGetter(ByVal key As String) As ReportMeasurement
        If _measurements IsNot Nothing AndAlso _measurements.Count > 0 Then
            If _measurements.ContainsKey(key) Then
                Return _measurements(key)
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Determines whether this instance has measurement.
    ''' </summary>
    ''' <returns><c>true</c> if this instance has measurement; otherwise, <c>false</c>.</returns>
    ''' <remarks></remarks>
    Public Function HasMeasurements() As Boolean
        Return Me._measurements IsNot Nothing AndAlso Me._measurements.Count > 0
    End Function

#End Region

#Region " PARSE"

    ''' <summary>
    ''' Gets or sets the error sentinel
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ErrorOccurred As Boolean

    ''' <summary>
    ''' Gets or sets the error message.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ErrorMessage As String

    ''' <summary>
    ''' Gets the record.
    ''' </summary>
    ''' <remarks></remarks>
    Public ReadOnly Property Record As String
        Get
            Return Me._records(_LastLocation)
        End Get
    End Property

    ''' <summary>
    ''' Gets the previous record.
    ''' </summary>
    ''' <remarks></remarks>
    Public ReadOnly Property PreviousRecord As String
        Get
            Return Me._records(_LastLocation - 1)
        End Get
    End Property

    ''' <summary>
    ''' Determines whether [is end module].
    ''' </summary>
    ''' <returns><c>true</c> if [is end module]; otherwise, <c>false</c>.</returns>
    ''' <remarks></remarks>
    Public Function IsEndModule() As Boolean
        Return Me.Record.Trim.StartsWith("End Sequence:", StringComparison.OrdinalIgnoreCase)
    End Function

    ''' <summary>
    ''' Lists the data.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ListData() As String
        Dim builder As New Text.StringBuilder
        If Me.HasRecords Then
            For Each r As KeyValuePair(Of String, String) In Me._ModuleRecords
                If builder.Length > 0 Then
                    builder.AppendLine()
                End If
                builder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "  {0}: {1}", r.Key, r.Value)
            Next
        End If
        If Me.HasMeasurements Then
            For Each m As KeyValuePair(Of String, ReportMeasurement) In Me._measurements
                If builder.Length > 0 Then
                    builder.AppendLine()
                End If
                builder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "    {0}:", m.Key)
                builder.AppendLine()
                builder.Append(m.Value.ListMeasurements)
            Next
        End If
        Return builder.ToString
    End Function

    ''' <summary>
    ''' Parses this instance.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Parse() As Boolean
        Me._ModuleRecords = New Dictionary(Of String, String)
        Me._measurements = New Dictionary(Of String, ReportMeasurement)
        Dim keyValue As KeyValuePair(Of String, String) = ParseRecord(Me.Record)
        Me.Name = keyValue.Key
        If Me.Name = "Power Output @ 1dB GCP 100 Mhz" Then
            'Stop
        End If
        _ModuleRecords.Add("Status", keyValue.Value)
        Do Until Me.IsEndModule
            Me.LastLocation += 1
            keyValue = ParseRecord(Me.Record)
            Dim m As ReportMeasurement = Nothing
            If keyValue.Key.Equals("Measurement", StringComparison.OrdinalIgnoreCase) Then
                If String.IsNullOrEmpty(keyValue.Value) Then
                    m = New ReportMeasurement(Me.LastLocation + 1, Me._records)
                Else
                    m = New ReportMeasurement(Me.LastLocation - 1, Me._records)
                End If
            ElseIf keyValue.Key.StartsWith("Measurement[", StringComparison.OrdinalIgnoreCase) Then
                m = New ReportMeasurement(Me.LastLocation, Me._records)
            ElseIf keyValue.Key.StartsWith("Begin Sequence", StringComparison.OrdinalIgnoreCase) Then
            ElseIf keyValue.Key.StartsWith("End Sequence", StringComparison.OrdinalIgnoreCase) Then
            ElseIf Not (String.IsNullOrEmpty(keyValue.Key) OrElse String.IsNullOrEmpty(keyValue.Value)) Then
                If Not _ModuleRecords.ContainsKey(keyValue.Key) Then
                    _ModuleRecords.Add(keyValue.Key, keyValue.Value)
                End If
            End If
            If m IsNot Nothing Then
                If m.Parse Then
                    Me._measurements.Add(m.Name, m)
                End If
                Me.LastLocation = m.LastLocation
            End If
        Loop
        Return True
    End Function

#End Region

End Class

''' <summary>
''' Enumerates thw module data elements
''' </summary>
''' <remarks></remarks>
Public Enum ModuleDataElement
    <ComponentModel.Description("None")> None
    <ComponentModel.Description("Module Time")> ModuleTime
    <ComponentModel.Description("Status")> Status
End Enum
