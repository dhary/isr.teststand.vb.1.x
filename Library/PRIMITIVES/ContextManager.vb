﻿Imports NationalInstruments.TestStand.Interop.API
Imports System.Runtime.InteropServices
''' <summary>
''' Manages getting the part information.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="05/27/2011" by="David Hary" revision="1.0.4164.x">
''' Created
''' </history>
Public Class ContextManager
    Implements IDisposable

#Region " CONSTRUCTORS AND DESTRACTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ContextManager" /> class.
    ''' </summary>
    Public Sub New()
        MyBase.new()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ContextManager" /> class.
    ''' </summary>
    ''' <param name="sequenceContext">The sequence context.</param>
    Public Sub New(ByVal sequenceContext As NationalInstruments.TestStand.Interop.API.SequenceContext)
        Me.new()
        _sequenceContext = sequenceContext
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method overridable (virtual) because a derived 
    ''' class should not be able to override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derviced class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return _isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            _isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by its disposing parameter.
    ''' If True, the method has been called directly or indirectly by a user's code--managed 
    ''' and unmanaged resources can be disposed. If disposing equals False, the method has been 
    ''' called by the runtime from inside the finalizer and you should not reference other 
    ''' objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                ' Free shared managed resources
                If disposing Then

                    If _sequenceContext IsNot Nothing Then
                        _sequenceContext = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method does not get called. 
    ''' It gives the base class the opportunity to finalize. Do not provide destructors 
    ''' in types derived from this class.
    ''' </summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here. Calling Dispose(false) is optimal in terms of
        ' readability and maintainability.
        Dispose(False)
        ' The compiler automatically adds a call to the base class finalizer 
        ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
        MyBase.Finalize()
    End Sub

#End Region

#Region " TEST STAND API "

    Private _sequenceContext As NationalInstruments.TestStand.Interop.API.SequenceContext

    ''' <summary>
    ''' Sets the step result error.
    ''' </summary>
    ''' <param name="sequenceContext">The sequence context.</param>
    ''' <param name="message">The message.</param>
    ''' <param name="errorCode">The error code.</param>
    ''' <remarks></remarks>
    Public Shared Sub SetStepResultError(ByVal sequenceContext As NationalInstruments.TestStand.Interop.API.SequenceContext, ByVal message As String, ByVal errorCode As Integer)
        If Not String.IsNullOrEmpty(message) AndAlso sequenceContext IsNot Nothing Then
            Dim stepResult As NationalInstruments.TestStand.Interop.API.Step = sequenceContext.Step
            stepResult.AsPropertyObject.SetValBoolean("Result.Error.Occurred", 0, True)
            stepResult.AsPropertyObject.SetValString("Result.Error.Msg", 0, message)
            stepResult.AsPropertyObject.SetValNumber("Result.Error.Code", 0, errorCode)
        End If
    End Sub

    ''' <summary>
    ''' Sets the step result error.
    ''' </summary>
    ''' <param name="sequenceContext">The sequence context.</param>
    ''' <param name="exception">The exception.</param>
    ''' <param name="errorCode">The error code.</param>
    ''' <remarks></remarks>
    Public Shared Sub SetStepResultError(ByVal sequenceContext As NationalInstruments.TestStand.Interop.API.SequenceContext, ByVal exception As System.Exception, ByVal errorCode As Integer)
        If exception IsNot Nothing Then
            ContextManager.SetStepResultError(sequenceContext, exception.ToString, errorCode)
        End If
    End Sub

    ''' <summary>
    ''' Sets the step result error.
    ''' </summary>
    ''' <param name="sequenceContext">The sequence context.</param>
    ''' <param name="exception">The exception.</param>
    ''' <remarks></remarks>
    Public Shared Sub SetStepResultError(ByVal sequenceContext As NationalInstruments.TestStand.Interop.API.SequenceContext,
                                         ByVal exception As System.Runtime.InteropServices.COMException)
        SetStepResultError(sequenceContext, exception, exception.ErrorCode)
    End Sub

    ''' <summary>
    ''' Sets the step result error.
    ''' </summary>
    ''' <param name="exception">The exception.</param>
    ''' <param name="errorCode">The error code.</param>
    ''' <remarks></remarks>
    Public Sub SetStepResultError(ByVal exception As System.Exception, ByVal errorCode As Integer)
        ContextManager.SetStepResultError(_sequenceContext, exception, errorCode)
    End Sub

    ''' <summary>
    ''' Sets the step result error.
    ''' </summary>
    ''' <param name="exception">The exception.</param>
    ''' <remarks></remarks>
    Public Sub SetStepResultError(ByVal exception As COMException)
        ContextManager.SetStepResultError(_sequenceContext, exception)
    End Sub


#End Region

End Class

''' <summary>
''' Enumerates error codes to pass to Test Stand. 
''' Negative error codes between -8999 and -8000 are scripting and coding errors, such as argument exceptions. 
''' Positive error codes between 5000 and 9999 are process and program errors, such as failure to open the database.
''' </summary>
''' <remarks></remarks>
Public Enum UserDefinedErrorCode
    ArgumentNullException = -8999
    UnknownException = 5000
    DataAccessException = 5001
    DataAccessError = 5002
End Enum
