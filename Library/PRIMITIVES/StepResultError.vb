''' <summary>
''' Step result error structure matching the test stand error structure.
''' </summary>
''' <license>
''' (c) 2011 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="05/27/2011" by="David Hary" revision="1.0.4164.x">
''' Created
''' </history>
Public Structure StepResultError
    Public Property Code As Integer
    Public Property Message As String
    Public Property Occurred As Boolean

#Region " EQUALS "

    ''' <summary>
    ''' Determines whether the specified <see cref="StepResultError" /> is equal to this instance.
    ''' </summary>
    ''' <param name="obj">The obj.</param><returns></returns>
    Public Overloads Function Equals(ByVal obj As StepResultError) As Boolean
        Return (Me.Message IsNot Nothing AndAlso Me.Message.Equals(obj.Message)) AndAlso
                        Me.Code.Equals(obj.Code) AndAlso
                        Me.Occurred.Equals(obj.Occurred)
    End Function

    ''' <summary>
    ''' Determines whether the specified <see cref="System.Object" /> is equal to this instance.
    ''' </summary>
    ''' <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param><returns>
    '''   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
    ''' </returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        If obj Is Nothing Then
            Return False
        End If
        Return Me.Equals(CType(obj, StepResultError))
    End Function

    ''' <summary>
    ''' Returns a hash code for this instance.
    ''' </summary><returns>
    ''' A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
    ''' </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Code
    End Function

    ''' <summary>
    ''' Implements the operator =.
    ''' </summary>
    ''' <param name="left">The left.</param>
    ''' <param name="right">The right.</param><returns>
    ''' The result of the operator.
    ''' </returns>
    Public Shared Operator =(ByVal left As StepResultError, ByVal right As StepResultError) As Boolean
        Return left.Equals(right)
    End Operator

    ''' <summary>
    ''' Implements the operator &lt;&gt;.
    ''' </summary>
    ''' <param name="left">The left.</param>
    ''' <param name="right">The right.</param><returns>
    ''' The result of the operator.
    ''' </returns>
    Public Shared Operator <>(ByVal left As StepResultError, ByVal right As StepResultError) As Boolean
        Return Not left.Equals(right)
    End Operator

#End Region

End Structure
